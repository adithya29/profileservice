package com.yBay.backgroundProcessing;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="customer")
public class Customer implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name= "id" , nullable = false)
    private int id;
    
    @Column(name = "name", nullable = false)
	private String name;
    
    @Column(name = "address")
	private String address;
    
    @Column(name = "email", nullable = false)
	private String email;
    
    @Column(name = "userName", nullable = false)
	private String userName;
    
    @Column(name = "passwd", nullable = false)
	private String passwd;
    
    private Order order;
    
	public Customer(String name, String address, String email, String userName, String passwd) {
		this.name = name;
		this.address = address;
		this.email = email;
		this.userName = userName;
		this.passwd = passwd;
	}
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPasswd() {
		return passwd;
	}


	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	
	@ManyToOne(cascade = CascadeType.ALL)
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String toString(){
		return String.format(
                "Customer[id=%d, name='%s', address='%s',email='%s',userName='%s',passwd='%s']",
                id,name,address,email,userName,passwd);
	}
}