package com.yBay.backgroundProcessing;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;




public class CustomerOrderRepository implements OrderRepository{

	@Autowired
	RestTemplate restTemplate;

	@Override
	public Customer getCustomerByUserName(String userName) {
		return restTemplate.getForObject("/projectManager",Customer.class,userName);
	}

	@Override
	public Order getOrderByDate(String userName, Date purchaseDate) {
		return restTemplate.getForObject("/orderSearch", Order.class,userName,purchaseDate);
	}
	


}
