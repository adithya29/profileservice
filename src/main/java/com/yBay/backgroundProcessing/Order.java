package com.yBay.backgroundProcessing;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name= "id" , nullable = false)
	int id;
	
	@Column(name = "cust_uName", nullable = false)
	String cust_userName;
	
	@Column(name = "purchaseDate")
	Date purchaseDate;
	
	@Column(name = "amount")
	int amount;
	
	@Column(name = "quantity")
	int quantity;
	
	@Column(name="itemDescription")
	List<String> itemDescription;
	
	@Column(name="status")
	String status;

	//private Customer customer;
	
	public Order(String cust_userName, Date purchaseDate, int amount, int quantity, List<String> itemDescription,
			String status) {
		super();
		this.cust_userName = cust_userName;
		this.purchaseDate = purchaseDate;
		this.amount = amount;
		this.quantity = quantity;
		this.itemDescription = itemDescription;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCust_userName() {
		return cust_userName;
	}

	public void setCust_userName(String cust_userName) {
		this.cust_userName = cust_userName;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public List<String> getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		List<String> items = Arrays.asList(itemDescription.split("\\s*"));
		this.itemDescription = items;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/*@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="cust_uName")
	public Customer getCustomer() {
		return customer;
	}*/

	/*public void setCustomer(Customer customer) {
		this.customer = customer;
	}*/
	
	
	
}
