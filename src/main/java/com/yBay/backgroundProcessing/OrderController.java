package com.yBay.backgroundProcessing;


import java.sql.Date;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
public class OrderController {
	@Autowired
	RestTemplate restTemplate;
	
	protected Logger logger = Logger.getLogger(OrderController.class.getName());
	
	@RequestMapping(value="/orderSearch", method = RequestMethod.GET)
	public String searchOrder(){
		return "orderSearch";
	}
	
	@RequestMapping(value="/orderSearch", method = RequestMethod.POST)
	public String getResult(@RequestParam("selDate") Date purchaseDate,@RequestParam("userName") String userName,ModelMap model){
		Order ord = restTemplate.getForObject("http://localhost:9090/orderResult", Order.class,purchaseDate,userName);
		model.addAttribute("order", ord);
		return "result";
	}
	
}
