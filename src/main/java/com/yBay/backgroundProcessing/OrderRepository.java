package com.yBay.backgroundProcessing;

import java.sql.Date;

import org.springframework.stereotype.Component;

@Component
public interface OrderRepository{
	public Customer getCustomerByUserName(String userName);
	public Order getOrderByDate(String userName,Date purchaseDate);
}
