<html>
<title>ybay.com/ProfileManager</title>
<head>Account Details</head>
<body>
<div class = "row">
	<h2>Please find and verify the details</h2>
	<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						Customer ID:
					</div>
					<div class="col-sm-9">
						${customer.id}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						Name:
					</div>
					<div class="col-sm-9">
						${customer.name}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						Address
					</div>
					<div class="col-sm-9">
						${customer.address}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						Email
					</div>
					<div class="col-sm-9">
						${customer.email}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						Username
					</div>
					<div class="col-sm-9">
						${customer.userName}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						Password
					</div>
					<div class="col-sm-9">
						${customer.passwd}
					</div>
				</div>
</div>
</body>
</html>