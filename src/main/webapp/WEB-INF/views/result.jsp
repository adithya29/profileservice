<html>
<title>ybay.com/ProfileManager</title>
<head>Order Details</head>
<body>
<div class = "row">
	<h2>Please find and verify the details</h2>
	<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						Order ID:
					</div>
					<div class="col-sm-9">
						${order.id}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						Name:
					</div>
					<div class="col-sm-9">
						${order.cust_uName}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						Total Price
					</div>
					<div class="col-sm-9">
						${order.amount}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						Quantity
					</div>
					<div class="col-sm-9">
						${order.quantity}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						Item Description
					</div>
					<div class="col-sm-9">
						${order.itemDescription}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						status
					</div>
					<div class="col-sm-9">
						${order.status}
					</div>
				</div>
</div>
</body>
</html>